#include <iostream>
#include "hero.hpp"

Hero::Hero(std::string new_name, int new_strength, int new_dexterity, int new_endurance, int new_intelligence, int new_charisma ){
    
    name = new_name;
    strength = new_strength;
    dexterity = new_dexterity;
    endurance = new_endurance;
    intelligence = new_intelligence;
    charisma = new_charisma;
    
}

void Type::set_type(std::string new_type){
    type = new_type;
}
void Type::increase(Hero& c){
    if(type == "Mage"){
        c.intelligence+=10;
    }else if (type == "Warrior"){
        c.endurance += 10;
    }else if (type == "Berserker"){
        c.strength +=10;
    }else if (type == "Thief"){
        c.dexterity +=10;
    }
}
    


std::string Hero::view(){
    std::string bio = "Name: " + name;
    bio += "\nStrength: " + std::to_string(strength);
    bio += "\nDexterity: " +std::to_string(dexterity);
    bio += "\nEndurance: " + std::to_string(endurance);
    bio += "\nIntelligence: " + std::to_string(intelligence);
    bio+= "\nCharisma: " + std::to_string(charisma)+"\n";
    return bio;
    
}


int Hero::save(){
    std::fstream file;
    file.open(name + ".txt", std::ios::out);
    file<<"Name: "<<name<<"   "<<"Strength: "<<strength<<"   "<<"Dexterity: "<<dexterity<<"   "<<"Endurance: " <<endurance<<"   "<<"Intelligence: "<<intelligence<<"   "<<"Charisma: "<<charisma<<"\n";
    file.close();
    return 0;
}
int Hero::load() {
    std::fstream f;
    f.open(name + ".txt", std::ios::in | std::ios::out);
    if (f.good() == true) {
        f >> name >> strength >> dexterity >> endurance >> intelligence >> charisma;
    }
    f.close();
    return 0;
}


