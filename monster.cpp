#include "monster.hpp"


std::string Monster::m_name(std::string new_name){
    name = new_name;
    return name;
}
int Monster::m_strength(int mon_strength){
    strength = mon_strength;
    return strength;
}
int Monster::m_dexterity(int mon_dexterity){
    dexterity = mon_dexterity;
    return dexterity;
}
int Monster::m_endurance(int mon_endurance){
    endurance = mon_endurance;
    return endurance;
}
int Monster::m_intelligence(int mon_intelligence){
    intelligence = mon_intelligence;
    return intelligence;
}
int Monster::m_charisma(int mon_charisma){
    charisma = mon_charisma;
    return charisma;
}
int Monster::save(){
    std::fstream file;
    file.open(name + ".txt", std::ios::out);
    file<<"Name: "<<name<<"   "<<"Strength: "<<strength<<"   "<<"Dexterity: "<<dexterity<<"   "<<"Endurance: " <<endurance<<"   "<<"Intelligence: "<<intelligence<<"   "<<"Charisma: "<<charisma<<"\n";
    file.close();
    return 0;
}
std::string Monster::view(){
    std::string bio = "Name: " + name;
    bio += "\nStrength: " + std::to_string(strength);
    bio += "\nDexterity: " +std::to_string(dexterity);
    bio += "\nEndurance: " + std::to_string(endurance);
    bio += "\nIntelligence: " + std::to_string(intelligence);
    bio+= "\nCharisma: " + std::to_string(charisma)+"\n";
    return bio;
    
}
